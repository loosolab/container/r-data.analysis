# r-data.analysis

A container that allows generic data analysis using R and R-Studio. 

### Obtaining the container

```
docker pull docker.gitlab.gwdg.de/loosolab/container/r-data.analysis:r3.6.2_bioc3.10
```

### Running the container

```
docker run -d -p 172.16.12.92:8787:8787 -e USERID=$UID -v /path/to/workspace:/home/rstudio/path/to/workspace docker.gitlab.gwdg.de/loosolab/container/r-data.analysis:r3.6.2_bioc3.10
```

### Available packages

All packages from the [rocker/tidyverse](https://hub.docker.com/r/rocker/tidyverse) plus

- `multipanelfigure`
- `ComplexHeatmap`
- `tinytex`
- `rmarkdown`
- `plotly`
- `rprojroot`
- `Matrix.utils`
- `rstudio/gt`
- `i2dash`

### Building the container

```
docker build --squash -t r-data.analysis:r3.6.2_bioc3.10 https://gitlab.gwdg.de/loosolab/container/r-data.analysis.git
```
